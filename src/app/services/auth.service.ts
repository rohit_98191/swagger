import { Injectable } from '@angular/core';
import { BaseService } from './base.services';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { BehaviorSubject, Observable } from "rxjs";
import {Router,Route} from '@angular/router'

@Injectable()
export class AuthService extends BaseService {

  isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());
  tokenSubject = new BehaviorSubject<any>(this.getToken());
  token="";

  constructor(private http: Http,public router:Router) {
    super();
   this.getUserToken().subscribe(key=>{
    //  console.log("user token",key)
    this.token=key;
   })
  }
  public validateCredantial(req){
    let headers = this.headers;
    let options = new RequestOptions({ headers: headers }); // Create a request option
    let body = JSON.stringify(req);
    return this.http.post(this.baseUrl+'admin/validateCredentials', body, options);
  }
  public logout(){
    localStorage.removeItem('userDetails');
    this.isLoginSubject.next(false);
    this.tokenSubject.next("");
    this.router.navigateByUrl('/login');


  }
  public forgotPassword(req){
    let headers = this.headers;
    let options = new RequestOptions({ headers: headers }); // Create a request option
    let body = JSON.stringify(req);
    return this.http.post(this.baseUrl+'admin/forgetPassword', body, options);
  }
  public changePassword(req){
    let headers = this.headers;
    headers.append('token',this.token);
    let options = new RequestOptions({ headers: headers }); // Create a request option
    let body = JSON.stringify(req);
    return this.http.post(this.baseUrl+'admin/changePassword', body, options);
  }

  private hasToken():boolean {
    return !!localStorage.getItem('userDetails');
  }

  private getToken(){
    var token=""
     var data = JSON.parse(localStorage.getItem('userDetails'))
     if(data){
       token= data.token;
     }
     return token;
  }
  Subjectlogin(user){
    var data = JSON.stringify(user);
    localStorage.setItem('userDetails',data);
    this.isLoginSubject.next(true);
    this.token=user.token;
    this.router.navigateByUrl('/home');
  }

  isLoggedIn( ):Observable<boolean> {
    return this.isLoginSubject.asObservable();
  }
  getUserToken():Observable<any>{
    return this.tokenSubject.asObservable();
  }

}
