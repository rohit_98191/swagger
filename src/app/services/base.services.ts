import { Headers, RequestOptions } from '@angular/http';
import { Component, Injectable, NgModule } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable()
export class BaseService {
    headers: Headers;
    public baseUrl;

    constructor() { 
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.baseUrl = "http://69.30.215.250:3000/"
    }

}