import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../app/services/auth.service';
import {AuthGuard} from '../app/services/authGuard';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Routes,RouterModule }  from '@angular/router';
import { AppComponent } from './app.component';
import { UserLoginComponent } from './user-login/user-login/user-login.component';

import {AppRoutes} from  '../app/app.routing';

import { HomeComponent } from './home/home/home.component'
import { ModalModule } from 'ngx-bootstrap/modal';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword/forgotpassword.component';





@NgModule({
  declarations: [
    AppComponent,
    UserLoginComponent,
    HomeComponent,
    ForgotpasswordComponent,
   
  ],
  imports: [
    ModalModule.forRoot(),
    HttpClientModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    
    RouterModule.forRoot(AppRoutes)
  ],
  providers: [AuthService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
