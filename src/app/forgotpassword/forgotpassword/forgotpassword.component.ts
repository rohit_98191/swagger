import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {


  complexForm: FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  loading=false;
  errmsg=null;

  constructor(public authService: AuthService, fb: FormBuilder) { 
    this.complexForm = fb.group({
      // We can set default values by passing in the corresponding value or leave blank if we wish to not set the value. For our example, we’ll default the gender to female.
      'email': [null,[Validators.required, Validators.pattern(this.emailPattern)]]
    })
  }

  submitForm(value: any): void {
    this.loading=true;
    this.authService.forgotPassword(value).subscribe(res => {
      var response = JSON.parse(res['_body']);
      console.log(response);
      if (response.status) {
        this.errmsg=response.message;
      }else{  
        this.errmsg=response.message;
      }
      this.loading=false;
      setTimeout(()=>{  
        this.errmsg="";
      },3000)
    })
  }

  

  ngOnInit() {
  }

}

