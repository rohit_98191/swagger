import { Component,TemplateRef, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import {AuthService} from '../../services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public modalRef: BsModalRef; 

  constructor(private modalService: BsModalService,private authService:AuthService) {}

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,{class: 'modal-lg'}); // {3}
  }

  ngOnInit() {
  }

logout(){
  this.authService.logout();
}
}
