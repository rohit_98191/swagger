import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  complexForm: FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  loading=false;
  err="";
  constructor(public authService: AuthService, fb: FormBuilder) {
   
     

    this.complexForm = fb.group({
      // We can set default values by passing in the corresponding value or leave blank if we wish to not set the value. For our example, we’ll default the gender to female.
      'email': [null,[Validators.required, Validators.pattern(this.emailPattern)]],
      'password': [null, Validators.required],
      "platform":"Mobile"
    })
  }

  ngOnInit() {

  }

  submitForm(value: any): void {
    this.loading=true;
    console.log(value);
    this.authService.validateCredantial(value).subscribe(res => {
      var response = JSON.parse(res['_body']);
      console.log(response);
      if (response.success) {
        this.loading=false;
        this.authService.Subjectlogin(response);
      }else{
        this.loading=false;
        this.err=response.message;
        setTimeout(()=>{
          this.err="";
        },1000)
      }
    })
  }

  onSubmit() {

  }
}
