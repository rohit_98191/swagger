import { UserLoginComponent } from './user-login/user-login/user-login.component';
import {AuthGuard} from '../app/services/authGuard';
import { Routes } from '@angular/router';
import { HomeComponent } from './home/home/home.component'
import {ForgotpasswordComponent} from '../app/forgotpassword/forgotpassword/forgotpassword.component'
export const AppRoutes: Routes=[
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path:'home',
        component:HomeComponent,
        canActivate:[AuthGuard]
    },
    {
        path:'login',
  
        component:UserLoginComponent,
      
   
    },
    {
        path: 'forgotPassword',
        
        component: ForgotpasswordComponent
    }
]