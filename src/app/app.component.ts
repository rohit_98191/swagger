import { Component,OnInit, TemplateRef } from '@angular/core';
import { Routes ,ActivatedRoute,Router} from '@angular/router';
import { AuthService } from './services/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  public preloginPage=false;
  public isLoggedIn = false;
  constructor(public _router: Router,public authService: AuthService) {
    this.authService.isLoggedIn().subscribe(flag => {
      // console.log(flag);
      this.isLoggedIn = flag;
    })
  }
  onActivate(component) {
    if(this._router.url.includes('booknow')){
     this.preloginPage=true;
    }else{
      this.preloginPage=false;
    }

    console.log(this.preloginPage);

  }
}
